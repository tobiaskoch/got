// Copyright 2023-2024 Tobias Koch. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package got is a go sample module.
//
//	package main
//
//	import (
//		"fmt"
//
//		"pkg.tk-software.de/got"
//	)
//
//	func main() {
//		fmt.Println(got.SayHello("Tobi"))
//	}
package got

import "fmt"

// SayHello greets the person with the name n.
func SayHello(n string) string {
	return fmt.Sprintf("Hello, %s", n)
}
