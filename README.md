# got
Package got is a go sample module.

```
package main

import (
	"fmt"

	"pkg.tk-software.de/got"
)

func main() {
	fmt.Println(got.SayHello("Tobi"))
}
```

## License
**got** © 2023-2024 [Tobias Koch](https://www.tk-software.de). Released under a [BSD-style license](https://gitlab.com/tobiaskoch/got/-/blob/main/LICENSE).